# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.core.management.base import BaseCommand

from main.models import Projeto
from main.tasks import queue_download


class Command(BaseCommand):
    help = u"Atualiza informações de todos os projetos cadastrados"

    def handle(self, *args, **options):
        for projeto in Projeto.objects.all():
            tag_ids = list(projeto.tags.values_list('id', flat=True))
            self.stdout.write((u"Enfileirando download dados do "
                               u"projeto {}").format(projeto))
            queue_download.delay(projeto.origem, projeto.id_site, tag_ids)
