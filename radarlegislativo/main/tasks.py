#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from celery import shared_task

from .models import Projeto, Tag
from .fetcher import fetch_senado_project
from .fetcher import fetch_camara_project

@shared_task
def queue_download(origem, id_no_site, tag_ids=None):
    if tag_ids is None:
        tag_ids = []

    tags = Tag.objects.filter(id__in=tag_ids)

    if origem == Projeto.CAMARA:
        fetch_camara_project(id_no_site, tags)

    elif origem == Projeto.SENADO:
        fetch_senado_project(id_no_site, tags)
