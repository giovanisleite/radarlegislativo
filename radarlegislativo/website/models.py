# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

O modelo ``models.WebsiteOptions`` foi feito para ter somente uma instância, que pode ser criada com uma URL do grafo que será linkada na página principal.

"""

from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError
from django.utils.encoding import python_2_unicode_compatible

class WebsiteOptions(models.Model):
    graph_url = models.CharField('URL do grafo', max_length=255)
    tracking_code = models.CharField('Código de rastreamento', max_length=1000)
    modified_date = models.DateTimeField('Última modificação', auto_now=True)


    class Meta:
        verbose_name = "Opções"
        verbose_name_plural = "Opções"

    def clean(self):
        if WebsiteOptions.objects.exists() and not self.pk:
            raise ValidationError('Só pode haver um objeto "Opções". Edite o que já existe.')
        return super(WebsiteOptions, self).clean()


