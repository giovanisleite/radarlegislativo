#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging

from django.core.management.base import BaseCommand

from tramitabot.bot import Tramitabot


class Command(BaseCommand):
    help = (u"Executa o Tramitabot para que ele fique esperando novas "
    u"interações de usuários.")

    def add_arguments(self, parser):
        parser.add_argument("--log-file", dest="log_file",
                    default="/tmp/tramitabot.log")
        parser.add_argument("--log-level", dest="log_level",
                    choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
                    default="INFO")

    def handle(self, *args, **options):
        log_level = logging.getLevelName(options["log_level"])
        logging.basicConfig(filename=options["log_file"], level=log_level)

        Tramitabot().listen()
