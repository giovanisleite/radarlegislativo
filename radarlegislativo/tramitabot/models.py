# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class TramitabotUser(models.Model):
    username = models.CharField(max_length=255, blank=True)
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)
    telegram_id = models.BigIntegerField()
    language_code = models.CharField(max_length=255, blank=True)
    has_blocked_us = models.BooleanField(default=False)

    def __str__(self):
        return "{} - {} {} - {}".format(self.username, self.first_name,
                self.last_name, self.telegram_id)
