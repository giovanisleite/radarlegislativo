# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from tramitabot.bot import Tramitabot
from tramitabot.models import TramitabotUser


def get_example_message():
    return {
        u'from': {
            u'username': u'rosaluxemburg',
            u'first_name': u'Rosa',
            u'last_name': u'Luxemburg',
            u'is_bot': False,
            u'language_code': u'en-US',
            u'id': 111111111
        },
        u'text': u'/ajuda',
        u'entities': [{
            u'length': 6,
            u'type': u'bot_command',
            u'offset': 0
        }],
        u'chat': {
            u'username': u'rosaluxemburg',
            u'first_name': u'Rosa',
            u'last_name': u'Luxemburg',
            u'type': u'private',
            u'id': 111111111
        },
        u'date': 1522268687,
        u'message_id': 262
    }


class TramitabotTest(TestCase):

    def test_registrar_usuario(self):
        bot = Tramitabot()
        msg = get_example_message()

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_deve_poder_registrar_usuario_sem_username(self):
        bot = Tramitabot()
        msg = get_example_message()
        del msg["from"]["username"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_deve_poder_registrar_usuario_sem_nome(self):
        bot = Tramitabot()
        msg = get_example_message()
        del msg["from"]["first_name"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_deve_poder_registrar_usuario_sem_sobrenome(self):
        bot = Tramitabot()
        msg = get_example_message()
        del msg["from"]["last_name"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_deve_poder_registrar_usuario_sem_language_code(self):
        bot = Tramitabot()
        msg = get_example_message()
        del msg["from"]["language_code"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())
