#!/bin/bash

SCRIPT_DIR="$(dirname $(readlink -f $0))"
PARENT_DIR="$(dirname $SCRIPT_DIR)"
CONTRIB_DIR="$PARENT_DIR/contrib"

if [ ! -d "$CONTRIB_DIR" ]
then
    mkdir "$CONTRIB_DIR"
fi

if [ -x "$CONTRIB_DIR/elasticsearch-2.4.6/bin/elasticsearch" ]
then
    echo "Elasticsearch já está disponível"
    exit 0
fi

if [ -z "$(type java 2> /dev/null)" ];
then
    echo "Java não está instalado, mas é um pré-requisito para o elasticsearch."
    echo "Instalando..."
    sudo apt-get update
    sudo apt-get install openjdk-8-jre
fi

if [ -z "$(type wget 2> /dev/null)" ];
then
    echo "Instalando wget..."
    sudo apt-get update
    sudo apt-get install wget
fi

wget -c "https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/2.4.6/elasticsearch-2.4.6.tar.gz" -O /tmp/elasticsearch-2.4.6.tar.gz

tar xzf /tmp/elasticsearch-2.4.6.tar.gz -C "$CONTRIB_DIR"

echo "Elasticsearch baixado e pronto para ser executado."
