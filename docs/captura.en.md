Configuration for the process of capture of draft bills
=======================================================

After the basic development environment is setup, just follow the
instructions below to configure the process that captures the information of
Draft Bills from the websites of Câmara and Senado.

If you have not yet configured the basic environment, please do it following
the [instructions][contributing].



Pre-requisites
--------------

Besides creating the [basic development environment][contributing], it is
also necessary to install RabbitMQ. In Debian GNU/Linux systems, you may
just install it (as root):

```
# apt-get install rabbitmq-server
```


Settings
--------

In order to the downloads be executed, it is necessary that celery is
running. The simplest way to do it is to run, from the root directory of the project:

```
$ make dev_celery
```

The command `python radarlegislativo/manage.py fetch_projeto` allows you to
download the projects directly from the command line (as a condition, celery
must be running). For this, it is necessary to inform the origin of the
project and its ID in the correspondent website. This is an example:

```
python radarlegislativo/manage.py fetch_projeto --origem camara --id 548066
```

This command will download the project with ID `548066` from the Câmara
website.

If you need to download various projects at once, use the option
`--from-file`` (informing a file in the same format than
[projetos-de-leis.yml][pls-yml]).

For example:

```
python radarlegislativo/manage.py fetch_projeto --from-file projetos-de-leis.yml
```

Troubleshooting
---------------

### `ConnectionError` / `Connection refused`

If you encounter an error similar to the following:

```
[2018-02-01 18:02:21,021: ERROR/ForkPoolWorker-1] celery_haystack.tasks.CeleryHaystackSignalHandler[2d83a5a6-c344-4a32-9f1e-c2f678b7994f]: ConnectionError(<urllib3.connec
tion.HTTPConnection object at 0x7f1b494e4cd0>: Failed to establish a new connection: [Errno 111] Connection refused) caused by: NewConnectionError(<urllib3.connection.HT$PConnection object at 0x7f1b494e4cd0>: Failed to establish a new connection: [Errno 111] Connection refused)
Traceback (most recent call last):
    [...]
ConnectionError: ConnectionError(<urllib3.connection.HTTPConnection object at 0x7f1b494e4cd0>: Failed to establish a new connection: [Errno 111] Connection refused) caus$
d by: NewConnectionError(<urllib3.connection.HTTPConnection object at 0x7f1b494e4cd0>: Failed to establish a new connection: [Errno 111] Connection refused)
```

It is possible that elasticsearch is not responding at port 9200. Verify
that it is running (the command `curl http://localhost:9200/` should return
information about the installation of elasticsearch).

If the problem persists, verify your connection to the Internet.


[contributing]: https://gitlab.com/codingrights/radarlegislativo/blob/master/CONTRIBUTING.en.md
[pls-yml]: https://gitlab.com/codingrights/radarlegislativo/blob/master/projetos-de-leis.yml
